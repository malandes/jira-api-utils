#!/usr/bin/python

import jira_connect


# REST API for a particular issue: https://its.cern.ch/jira/rest/api/2/issue/PROJECTALT-11

def print_issue (issue):

   jira = jira_connect.get_jira_client()
   print "Information about Issue %s" % (issue)
   issue = jira.issue(issue)
   print "Summary: %s"      % (issue.fields.summary)
   print "Last Updated: %s" % (issue.fields.updated)
   print "Assignee: %s"     % (issue.fields.assignee.displayName)
   print "Status: %s"       % (issue.fields.status.name)
   # Custom fields for WBS Gantt Chart
   print "Start Date: %s"           % (issue.fields.customfield_14006)
   print "Finish Date: %s"          % (issue.fields.customfield_14007)
   print "Progress: %s"             % (issue.fields.customfield_14010)
   print "Units: %s"                % (issue.fields.customfield_14012)
   print "Baseline start date: %s"  % (issue.fields.customfield_14008)
   print "Baseline finish date: %s" % (issue.fields.customfield_14009)
   print "Milestone: %s"            % (issue.fields.customfield_14011)
   print "Manually scheduled: %s"   % (issue.fields.customfield_14013)
   labels = issue.fields.labels
   for label in labels:
      print "Label: %s"            % (label)



