#!/usr/bin/python

from jira import JIRA

CERN_CA_BUNDLE = '/etc/pki/tls/certs/ca-bundle.crt'

def get_jira_client():
   return JIRA('https://its.cern.ch/jira',
               basic_auth=('user', 'password'),
               options={'check_update': False, 'verify': CERN_CA_BUNDLE})
             
             