# jira-api-utils

Set of python libraries and functions to programatically interact and manipulate JIRA projects and issues

Disclaimer: Note that this repository is not maintained by the JIRA Service Managers. It's a community effort based on voluntary work.



Instructions to make use of these functions

1. Download JIRA Python API from  https://pypi.org/project/jira/
2. Create a service account and log in once with it to JIRA
4. Download the jira-api command and all the jira-*.py libraries
5. Make sure you define the necessary paths in your PYTHONPATH to access both the JIRA API and the libraries you have just downloaded
6. Make sure you define your service account name and password in jira_connect.py