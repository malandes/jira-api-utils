#!/usr/bin/python

import jira_project

def print_project_labels_csv (project):

   issues=jira_project.get_project_issues (project)
   print "Key,Summary,Budget Code, Group, Cost"
   for issue in issues:

      label_dict = { "BudgetCode" : "",
                     "Group"      : "",
                     "Cost"       : ""
                  }

      for label in issues[issue]["Labels"]:
         if  label.find("CB_") > -1:
            label_dict["BudgetCode"] = label
         elif label.find("G_") > -1:
            label_dict["Group"] = label
         elif label.find("M_") > -1:
            label_dict["Cost"] = label

      print "%s,%s,%s,%s,%s" % (issues[issue]["Key"],issues[issue]["Summary"],label_dict["BudgetCode"],label_dict["Group"],label_dict["Cost"])
